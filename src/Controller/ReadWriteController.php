<?php

namespace App\Controller;

use App\Service\ReadCSV;
use App\Service\WriteCSV;
use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReadWriteController extends AbstractController
{
    /**
     * @Route("/read", name="read")
     */
    public function index(): Response
    {
        $data = [];
        $reader = new ReadCSV("./files/addresses.csv");
        $data = $reader->read();
        //dd($reader->read());

        $writer = new WriteCSV("./files/results.csv");
        $nbRow = $writer->write($data);

        return $this->render('read_write/index.html.twig', [
            'controller_name' => 'Amandine',
            'name' => $nbRow,
        ]);
    }

    /**
     * @Route("/read2", name="read2")
     */
    public function index2(): Response
    {
        $dataCopy = [];
        $reader = new ReadCSV("./files/source.csv");
        $data = $reader->read();

        for($i = 0;$i < count($data); $i++){
            $line = $data[$i];
            if($i >= 1){
                $line[2] = "Lyon";
                $line[3] = "69005";
            }
            $dataCopy[] = $line;
        }

        $writer = new WriteCSV("./files/target.csv");
        $nbRow = $writer->write($data);

        return $this->render('read_write/index.html.twig', [
            'controller_name' => 'Amandine',
            'nbRow' => $nbRow
        ]);
    }

    /**
     * @Route("/read3", name="read3")
     */
    public function index3(CallApiService $callApiService): Response
    {
        $dataCopy = [];

        $reader = new ReadCSV("./files/source.csv");
        $data = $reader->read();
        for($i = 0;$i < 3; $i++){
            $line = $data[$i];
            if($i >= 1){
                $origin = $line[0]." ".$line[1];
                $localite_cible = $line[2] ?? "Orgeval";
                $cp_cible = $line[3] ?? "78630";
                $result = $callApiService->getDistanceApi($origin, $localite_cible . " " . $cp_cible);
                //dd($result);
                if(isset($result["rows"][0])){
                    $line[2] = $line[2] ?? "Orgeval";
                    $line[3] = $line[3] ?? "78630";
                    $line[4] = $result["rows"][0]["elements"][0]["distance"]["text"];
                    $line[5] = $result["rows"][0]["elements"][0]["duration"]["text"];
                }
            }
            $dataCopy[] = $line;
        }
        //dd($dataCopy);

        $writer = new WriteCSV("./files/target.csv");
        $nbRow = $writer->write($dataCopy);

        return $this->render('read_write/index.html.twig', [
            'controller_name' => 'Amandine',
            'nbRow' => $nbRow
        ]);
    }
}
