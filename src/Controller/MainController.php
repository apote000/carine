<?php

namespace App\Controller;

use App\Form\Dto\Upload;
use App\Form\Type\UploadType;
use App\Service\CallApiService;
use App\Service\ReadCSV;
use App\Service\WriteCSV;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class MainController extends AbstractController
{

    /**
     * @Route("/main", name="main")
     */
    public function index(Request $request, SluggerInterface $slugger, CallApiService $callApiService): Response
    {
        $upload = new Upload();

        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /** @var UploadedFile $data */
            $data = $form->get('file')->getData();

            if ($data) {
                $originalFilename = pathinfo($data->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = strtolower($safeFilename . '-' . uniqid()) . '.' . "csv";
                //dd($newFilename);

                // Move the file to the directory where brochures are stored
                try {
                    $data->move(
                        $this->getParameter('upload_file'),
                        $newFilename
                    );

                    $dataCopy = [];

                    $reader = new ReadCSV("./files/" . $newFilename);
                    $data = $reader->read();

                    for ($i = 0; $i < count($data); $i++) {
                        $line = $data[$i];
                        if ($i >= 1) {
                            $origin = trim($line[0]) . " " . trim($line[1]);
                            $localite_cible = $line[2] ?? "Orgeval";
                            $cp_cible = $line[3] ?? "78630";
                            $result = $callApiService->getDistanceApi($origin, $localite_cible . " " . $cp_cible);
                            //dd($result);
                            if (isset($result["rows"][0]) &&
                                isset($result["rows"][0]["elements"][0]["distance"]) &&
                                isset($result["rows"][0]["elements"][0]["duration"])) {
                                $line[2] = $line[2] ?? "Orgeval";
                                $line[3] = $line[3] ?? "78630";
                                $line[4] = $result["rows"][0]["elements"][0]["distance"]["text"];
                                $line[5] = $result["rows"][0]["elements"][0]["duration"]["text"];
                            }
                        }
                        $dataCopy[] = $line;
                    }
                    $writer = new WriteCSV("./files/result_ok.csv");
                    $writer->write($dataCopy);

                    return $this->redirectToRoute('main', [
                        'task' => 'done',
                        '_fragment' => 'portfolio'
                    ] );

                } catch (FileException $e) {
                    dd($e);
                }
            }

        }
        $lien = $this->getParameter('upload_file') . "./files/result_ok.csv";

        return $this->renderForm('main/index.html.twig', [
            'form' => $form,
            'lien' => $this->getParameter('site_url'),
        ]);

    }

    /**
     * @Route("file/download/{fileId}", name="download_attached_file", methods={"GET"})
     */
    public function downloadFile(string $fileId) : BinaryFileResponse
    {
        $attachedDoc = $this->getParameter('upload_file').'/'.$fileId;

        if ($attachedDoc) {

            // This should return the file to the browser as response
            $response = new BinaryFileResponse($attachedDoc);

            $response->headers->set('Content-Type', 'text/plain');

            // Set content disposition inline of the file
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $response->getFile()->getFilename()
            );
            return $response;
        }
    }
}
