<?php

namespace App\Form\Dto;

class Upload
{
    private string $file;

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;
        return $this;
    }
}