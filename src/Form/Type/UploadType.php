<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

final class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', FileType::class, [
                "mapped" => false,
                'label' => false,
                'attr' => [
                    'class' => 'btn btn-info',
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'send',
                'attr' => [
                    'class' => 'btn btn-primary',
                    'style' => 'margin-top: 30px'
                ]
            ])
        ;
    }
}
