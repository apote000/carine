<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;

class CallApiService
{
    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getGeocodeApi(string $address): array
    {
        $response = $this->client->request(
            'GET',
            'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyCK9zYPp6z1-FPA1CdMsCqcrnnxGY1oED4'
        );
        //dd($response);
        return $response->toArray();
    }

    public function getDistanceApi(string $origin, string $destination): array
    {
        $response = $this->client->request(
            'GET',
            'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origin.'&destinations='.$destination.'&key=AIzaSyCK9zYPp6z1-FPA1CdMsCqcrnnxGY1oED4'
        );
        return $response->toArray();
    }
}