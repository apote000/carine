<?php

namespace App\Service;

class WriteCSV
{
    private string $file;
    private string $separator;
    private int $length;
    private array $results;


    public function __construct(string $file)
    {
        $this->file = $file;
        $this->separator = ";";
        $this->results = [];
        $this->length = 1000;
    }

    public function write(array $content)
    {
        $stream = fopen($this->file, "w+");
        $result = 0;
        foreach ($content as $row) {
            $result = fputcsv($stream, $row, $this->separator);
        }
        return $result;
    }
}