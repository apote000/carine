<?php

declare(strict_types=1);

namespace App\Service;

final class ReadCSV
{
    private string $file;
    private string $separator;
    private int $length;
    private array $results;


    public function __construct(string $file)
    {
        $this->file = $file;
        $this->separator = ";";
        $this->results = [];
        $this->length = 1000;
    }

    public function read(): array
    {
        $stream = fopen($this->file, "r");
        while (($data = fgetcsv($stream, $this->length, $this->separator)) !== FALSE) {
            $this->results[] = $data;
        }
        fclose($stream);
        return $this->results;
    }
}
